import java.math.BigInteger;
import java.util.Random;

import org.vu.contest.ContestEvaluation;


public class BitFlip extends GA {

	public BitFlip(int populationSize, int dimSize, Random rnd, EvalWrapper ev, int evlim){
		super(populationSize, dimSize, rnd, ev, evlim);
	}

	
	public void mutate(int ind) {
		
		String[] newIndividual = getGenome(ind);
		
		for (int i = 0; i < dims; i++){
			int index = (int) (rnd.nextDouble()*newIndividual[i].length()); //just random bit flip (1 bit only)
			newIndividual[i] = newIndividual[i].substring(0, index) + flip(newIndividual[i].substring(index, index+1)) 
					         + newIndividual[i].substring(index+1, newIndividual[i].length());
		}
		
		population.get(ind).setPhenome(toPhenome(newIndividual));
		
	}
	private String flip(String s){
		return s.equals("0")? "1":"0";
	}
	

	
	@Override
	public Individual crossover(int crossoverMethod) {

		// Random selection of individuals to crossover/breed
		int ind1 = rnd.nextInt(population.size());
		int ind2 = rnd.nextInt(population.size());
		while (ind1 == ind2) {
			ind2 = rnd.nextInt(population.size());
		}

		double[] newIndividual = new double[dims];
		
		for (int i = 0; i < dims; i++) {
        	switch(crossoverMethod){
        	case DISCRETE: //DISCRETE
	        	if (rnd.nextDouble() <= crossoverRate)
	                newIndividual[i] =  population.get(ind1).getPhenome()[i];
	            else 
	            	newIndividual[i] =  population.get(ind2).getPhenome()[i];
	            break;
        	case MEAN: //MEAN
	                newIndividual[i] = 0.5*( population.get(ind1).getPhenome()[i] +  population.get(ind2).getPhenome()[i]);
	            break;
        	case RANDOM: //RANDOM
	        	double p = rnd.nextDouble();
	                newIndividual[i] = p* population.get(ind1).getPhenome()[i] + (1-p)* population.get(ind2).getPhenome()[i];
	            break;
	            
	            
	        }
        }
		
        Individual ind = new Individual(rnd,dims);
        ind.setPhenome(newIndividual);
        return ind;
	}


	@Override
	public String[] getGenome(int ind) {
		String[] genome  = new String[dims];
		double[] phenome = population.get(ind).getPhenome();

		for (int i = 0; i < dims; i++){
			genome[i] = Long.toBinaryString(Double.doubleToRawLongBits(phenome[i]));
		}
		
		return genome;
	}


	@Override
	public double[] toPhenome(Object genome) {
		double[] phenome = new double[dims];
		String[] s = (String[]) genome;
		for (int i = 0; i < dims; i++){
			phenome[i] = Double.longBitsToDouble(new BigInteger(s[i], 2).longValue());
		}
		return phenome;
	}

	

}
