import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public abstract class GA {
	
	protected int    pSize;
	protected int    dims;
	protected Random rnd;
	protected ArrayList<Individual> population;
	protected EvalWrapper evaluation;
	protected int evaluations_limit;
    protected int nParents = 2;
    protected boolean elitism = false;    
    protected double crossoverRate = 0.5;
    protected double mutationRate = 0.015;
    public static final int DISCRETE = 0, MEAN = 1, RANDOM = 2;
    protected int crossoverMethod = DISCRETE;
    
	public GA(int populationSize, int dimSize, Random rnd, EvalWrapper ev, int evlim){
		pSize     = populationSize;
		dims      = dimSize;
		this.rnd  = rnd;
		population = new ArrayList<Individual>(pSize);
		evaluation = ev;
		evaluations_limit = evlim;
		for (int i = 0; i < pSize; i++){
			Individual ind = new Individual(rnd, dims);
			ind.randInit();
        	population.add(ind);
        }

	}

	public void randInit(Individual ind){
		randInitPhenome(Individual ind);
		randInitGenome(Individual ind);
	}

	public void randInitPhenome(){
		phenome  = new double[phLength];
		for(int i = 0; i < phLength; i++){
			phenome[i] = rnd.nextDouble()*(phMax-phMin) + phMin;
		}
	}

	public void randInitGenome(){
		genome  = new double[phLength];
		for(int i = 0; i < phLength; i++){
			genome[i] = rnd.nextDouble()*(phMax-phMin) + phMin;
		}
	}

	// Removing this function as it just adds confusion for me
//	public Individual getIndividual(int ind) {
//		return population.get(ind);
//	}

	public void solve(){
        int evals = 0;
        int elitismOffset = 0;
    	if (elitism){
    		elitismOffset = 1;
    	}
		for (int i = 0; i < pSize; i++){ //evaluate fitness
			Individual ind = population.get(i);
            ind.setFitness((Double) evaluation.evaluate(ind.getPhenome()));
            evals++;
		}
		
		sortPopulation();
        while(evals<evaluations_limit &&  population.get(0).getFitness() != 10.0){
        	
        	ArrayList<Individual> newPopulation = new ArrayList<Individual>(pSize);
        	
        	if (elitism){
        		newPopulation.add(population.get(0));
        	}
        	
        	for (int i = elitismOffset; i < pSize; i++){
        		newPopulation.add((Individual) crossover(0, 1,crossoverMethod));
        	}
        	
        	population = newPopulation;
        	
        	for (int i = elitismOffset; i < pSize; i++)
        		mutate(i); 
            
            
        	for (int i = 0; i < pSize; i++){ //evaluate fitness
    			Individual ind = population.get(i);
                ind.setFitness((Double) evaluation.evaluate(ind.getPhenome()));
                evals++;
    		}
        	sortPopulation();
        	
           
        }
	}
	public abstract void mutate(int ind);
	public abstract Individual crossover(int ind1,int ind2, int crossoverType);
	public abstract Object getGenome(int ind);
	public abstract double[] toPhenome(Object genome);

	public void sortPopulation() {
		Collections.sort(population);		
	}
	public void setCrossoverMethod(int method){
		crossoverMethod = method;
	}
		
	
}
