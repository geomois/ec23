import org.vu.contest.ContestEvaluation;

/**
 * Created by liam on 18-9-16.
 *
 * Wrapper class so that we can evaluate on just 1 dimension if we want...
 *
 */
public class EvalWrapper {

    private ContestEvaluation ev;
    private int[] dimsToEval;
    private boolean allDims;
    public double[] currentSoln;

    public EvalWrapper(ContestEvaluation eval, int[] dimsToEvaluate, double[] currSoln){
        this.ev = eval;
        this.dimsToEval = dimsToEvaluate;
        this.currentSoln = currSoln;
        if (dimsToEval.length == 10 || dimsToEvaluate.length == 0) {
            this.allDims = true;
        } else {
            this.allDims = false;
        }
    }

    public Object evaluate(Object soln) {

        if (this.allDims) {

            return(ev.evaluate(soln));

        } else {

            double[] solution = (double[]) soln;
            int j = 0;
            for (int i : dimsToEval) {
                currentSoln[i] = solution[j];
                j += 1;
            }
            return ev.evaluate(currentSoln);

        }
    }
}
