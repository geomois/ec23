
import java.util.Random;


public class CMAES extends GA {
	
    public CMAES(int populationSize, int dimSize, Random rnd,
                 EvalWrapper ev, int evlim) {
        super(populationSize, dimSize, rnd, ev, evlim);
        
        
        for(int i = 0; i < populationSize; i++){
        	population.get(i).setExtended(getInitGenome());
        }
        

    }
    
    public double[] getInitGenome() {

        double[] initGenome = new double[dims];

        for (int i = 0; i < dims; i++) {
            // Genome is an array of standard deviations - one for each dim.
            // Initialise these between 0.5 and 1.5...
            initGenome[i] = rnd.nextDouble()+0.05;
        }

        return initGenome;
    }

    
    @Override
    public void mutate(int ind) {
    	Individual nind = population.get(ind);
        double[] genomeOut  = nind.getExtended();
        double[] phenomeOut = nind.getPhenome();
        // See page 60 of textbook!
        double globGauss = rnd.nextGaussian();
        double tau2 = 1.0 / Math.sqrt(2.0 * dims);
        double tau1 = 1.0 / Math.sqrt(2.0 * Math.sqrt(dims));

        // Hyperparam eta is the minimum allowable standard deviation
        double eta = 0.05;

        for (int i = 0; i < dims; i++) {
        	if(rnd.nextDouble() < mutationRate){
	        	double itGauss = rnd.nextGaussian();
	            genomeOut[i] *= Math.exp(globGauss * tau2 + itGauss * tau1);
	            if (genomeOut[i] < eta) {
	                genomeOut[i] = eta;
	            }
	            phenomeOut[i] += genomeOut[i] * itGauss;
        	}
        }

        nind.setExtended(genomeOut);
        nind.setPhenome(phenomeOut);

    }
    

    @Override
    public double[] toPhenome(Object genome) {
        return (double[]) genome;
    }
    @Override
	public Individual crossover(int ind1, int ind2, int crossoverType) {
		double[] newIndividual = new double[dims];
		double[] sigmas = new double[dims];		
        for (int i = 0; i < dims; i++) {
        	switch(crossoverMethod){
        	case DISCRETE: //DISCRETE
	        	if (rnd.nextDouble() <= crossoverRate)
	                newIndividual[i] = getGenome(ind1)[i];
	            else 
	            	newIndividual[i] = getGenome(ind2)[i];
	            break;
        	case MEAN: //MEAN
	                newIndividual[i] = 0.5*(getGenome(ind1)[i] + getGenome(ind2)[i]);
	            break;
        	case RANDOM: //RANDOM
	        	double p = rnd.nextDouble();
	                newIndividual[i] = p*getGenome(ind1)[i] + (1-p)*getGenome(ind2)[i];
	            break;
        	
        		
	            
	        }
        }
        for (int i = 0; i < dims; i++) {
        	switch(crossoverMethod){
        	case DISCRETE: //DISCRETE
	        	if (rnd.nextDouble() <= crossoverRate)
	                sigmas[i] = getExtGenome(ind1)[i];
	            else 
	            	sigmas[i] = getExtGenome(ind2)[i];
	            break;
        	case MEAN: //MEAN
        		sigmas[i] = 0.5*(getExtGenome(ind1)[i] + getExtGenome(ind2)[i]);
	            break;
        	case RANDOM: //RANDOM
	        	double p = rnd.nextDouble();
	        	sigmas[i] = p*getExtGenome(ind1)[i] + (1-p)*getExtGenome(ind2)[i];
	            break;
	            
	            
	        }
        }

        Individual ind = new Individual(rnd,dims);
        ind.setPhenome(newIndividual);
        ind.setExtended(sigmas);
        
        return ind;
	}

	@Override
	public double[] getGenome(int ind) {
		return population.get(ind).getPhenome();
	}
	public double[] getExtGenome(int ind){
		return population.get(ind).getExtended();
	}
	


}