//import java.math.BigInteger;
//import java.util.Random;
//
//
//public class GATest extends GA {
//
//	public GATest(int populationSize, int dimSize, Random rnd, EvalWrapper ev, int evlim){
//		super(populationSize, dimSize, rnd, ev, evlim);
//	}
//
//	@Override
//	public void mutate(int ind) {
//
//		double[] newIndividual = population.get(ind).getGenome();
//
//		for (int i = 0; i < dims; i++){
//			String s = Long.toBinaryString(Double.doubleToRawLongBits(newIndividual[i]));
//			int index = (int) (rnd.nextDouble()*s.length()); //just random bit flip (1 bit only)
//			String newBitString = s.substring(0, index) + flip(s.substring(index, index+1)) + s.substring(index+1, s.length());
//			newIndividual[i] = Double.longBitsToDouble(new BigInteger(newBitString, 2).longValue());
//		}
//
//		population.get(ind).setGenome(newIndividual);
//
//	}
//	private String flip(String s){
//		return s.equals("0")? "1":"0";
//	}
//	@Override
//	public double[] crossover(int ind1, int ind2) {
//		double[] newIndividual = new double[dims];
//
//        for (int i = 0; i < dims; i++) {
//            // Crossover
//            if (Math.random() <= crossoverRate) {
//                newIndividual[i] = population.get(ind1).getGenome()[i];
//            } else {
//            	newIndividual[i] = population.get(ind2).getGenome()[i];
//            }
//
//
//        }
//        return newIndividual;
//	}
//
//
//}
