import java.util.ArrayList;
import java.util.Random;


public class NonUniform extends GA {

	private int T;
	private static double b = 5.0;
	private int t;
	public NonUniform(int populationSize, int dimSize, Random rnd,
			EvalWrapper ev, int evlim) {
		super(populationSize, dimSize, rnd, ev, evlim);
				T = evlim/populationSize - 1;
		setCrossoverMethod(RANDOM);
	}

	@Override
	public void solve(){
        int evals = t = 0;
        int elitismOffset = 0;
    	if (elitism){
    		elitismOffset = 1;
    	}
		for (int i = 0; i < pSize; i++){ //evaluate fitness
			Individual ind = this.getIndividual(i);
            ind.setFitness((Double) evaluation.evaluate(ind.getPhenome()));
            evals++;
		}
		
		sortPopulation();
        while(evals<evaluations_limit &&  population.get(0).getFitness() != 10.0){
        	
        	ArrayList<Individual> newPopulation = new ArrayList<Individual>(pSize);
        	
        	if (elitism){
        		newPopulation.add(population.get(0));
        	}
        	
        	for (int i = elitismOffset; i < pSize; i++){
        		newPopulation.add(crossover(0, 1,crossoverMethod));
        	}
        	
        	population = newPopulation;
        	
        	for (int i = elitismOffset; i < pSize; i++)
        		mutate(i); 
            t++;
            
        	for (int i = 0; i < pSize; i++){ //evaluate fitness
    			Individual ind = getIndividual(i);
                ind.setFitness((Double) evaluation.evaluate(ind.getPhenome()));
                evals++;
    		}
        	sortPopulation();
        	
           
        }
	}
	@Override
	public void mutate(int ind) {
		
		double[] newIndividual = getGenome(ind);
		double UB = Individual.getUpperBound();
		double LB = Individual.getLowerBound();
		for (int i = 0; i < dims; i++){
			if(rnd.nextDouble() < mutationRate){
				if(rnd.nextBoolean()){
					newIndividual[i] += delta(t, UB - newIndividual[i]);
				}else{
					newIndividual[i] -= delta(t, newIndividual[i] - LB);
				}
				if(newIndividual[i] > UB) newIndividual[i] = UB;
				else if(newIndividual[i] < LB) newIndividual[i] = LB;

			}

		}
		
		population.get(ind).setPhenome(newIndividual); //if phenome =/= genome, use toPhenome method.

	}

	@Override
	public Individual crossover(int ind1, int ind2, int crossoverMethod) {
		double[] newIndividual = new double[dims];
		
        for (int i = 0; i < dims; i++) {
        	switch(crossoverMethod){
        	case DISCRETE: //DISCRETE
	        	if (rnd.nextDouble() <= crossoverRate)
	                newIndividual[i] = getGenome(ind1)[i];
	            else 
	            	newIndividual[i] = getGenome(ind2)[i];
	            break;
        	case MEAN: //MEAN
	                newIndividual[i] = 0.5*(getGenome(ind1)[i] + getGenome(ind2)[i]);
	            break;
        	case RANDOM: //RANDOM
	        	double p = rnd.nextDouble();
	                newIndividual[i] = p*getGenome(ind1)[i] + (1-p)*getGenome(ind2)[i];
	            break;
	            
	            
	        }
        }

        Individual ind = new Individual(rnd,dims);
        ind.setPhenome(newIndividual);
        return ind;
	}

	private double delta(int t, double y) {
		return (y*(1.0-Math.pow(rnd.nextDouble(),Math.pow((1.0 -t/(double) T), b))));
	}

	@Override
	public double[] getGenome(int ind) {
		return population.get(ind).getPhenome();
	}
	@Override
	public double[] toPhenome(Object genome) {
		return (double[])genome;
	}
	
  

}
