import java.util.Random;


public class Individual implements Comparable<Individual>{
	
    private double        fitness;
    private double[]      phenome;
    private double[]      genome;
    private double phMin   = -5.0;
    private double phMax   =  5.0;
    private int phLength;
    private Random rnd;
    
    public Individual(Random rnd, int phenomeLength){
    	fitness = 0.0;
    	phLength = phenomeLength;
        this.rnd = rnd;
    }
    
	public int compareTo(Individual indv) {
		double fit1 = indv.getFitness();
		double fit2 = fitness;
		if(Double.isNaN(fit1)) fit1 = -9999999.0;
		if(Double.isNaN(fit2)) fit2 = -9999999.0;
		return new Double(fit1).compareTo(fit2);
	}
	public double getFitness(){
		return fitness;
	}
	public void setFitness(double fitness){
		this.fitness = fitness;
	}
	public double[] getPhenome(){
		return phenome;
	}
	public void setPhenome(double[] genome){
		this.phenome = genome;
	}
	public static double getUpperBound(){
		return max;
	}
	public static double getLowerBound(){
		return min;
	}
	public double[] getExtended() {
		return extended;
	}
	public void setExtended(double[] extended) {
		this.extended = extended;
	}
	
	

}
