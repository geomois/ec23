import java.util.Random;



public class GaussianUniform extends GA{

	public GaussianUniform(int populationSize, int dimSize, Random rnd,
			EvalWrapper ev, int evlim) {
		super(populationSize, dimSize, rnd, ev, evlim);
		
	}


	@Override
	public void mutate(int ind) {
		
		double[] newIndividual = getGenome(ind);
		double UB = Individual.getUpperBound();
		double LB = Individual.getLowerBound();
		double std = 0.3;
		for (int i = 0; i < dims; i++){
			if(rnd.nextDouble() < mutationRate){
				newIndividual[i] +=  rnd.nextGaussian()*std;
				if(newIndividual[i] > UB) newIndividual[i] = UB;
				else if(newIndividual[i] < LB) newIndividual[i] = LB;
			}

		}
		
		population.get(ind).setPhenome(newIndividual); //if phenome =/= genome, use toPhenome method.
		
	}

	@Override
	public Individual crossover(int ind1, int ind2, int crossoverMethod) {
		double[] newIndividual = new double[dims];
		
        for (int i = 0; i < dims; i++) {
        	switch(crossoverMethod){
        	case DISCRETE: //DISCRETE
	        	if (rnd.nextDouble() <= crossoverRate)
	                newIndividual[i] = getGenome(ind1)[i];
	            else 
	            	newIndividual[i] = getGenome(ind2)[i];
	            break;
        	case MEAN: //MEAN
	                newIndividual[i] = 0.5*(getGenome(ind1)[i] + getGenome(ind2)[i]);
	            break;
        	case RANDOM: //RANDOM
	        	double p = rnd.nextDouble();
	                newIndividual[i] = p*getGenome(ind1)[i] + (1-p)*getGenome(ind2)[i];
	            break;
	            
	            
	        }
        }

        Individual ind = new Individual(rnd,dims);
        ind.setPhenome(newIndividual);
        return ind;
	}



	@Override
	public double[] getGenome(int ind) {
		return population.get(ind).getPhenome();
	}
	@Override
	public double[] toPhenome(Object genome) {
		return (double[])genome;
	}

}
