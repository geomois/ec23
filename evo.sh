#!/bin/bash 

declare -a arr=("SphereEvaluation" "RastriginEvaluation" "FletcherPowellEvaluation" "AckleyEvaluation" "LangermanEvaluation" "SchwefelEvaluation") 
 
f=results.txt

echo Starting evaluations > "$f"
echo "" >> "$f"
for ev in "${arr[@]}";
do
  echo "$ev" >> "$f"
  summ=0
  for i in `seq 1 5`;
  do
    echo it "$i" >> "$f"
    out="$(java -jar testrun.jar -submission=player23 -evaluation=$ev -seed=$RANDOM)" 
    echo "${out}" >> "$f"
    arr=($out)
    summ=$(bc <<< "${arr[1]}+$summ")
  done
  mean=$(bc <<< "scale=15;$summ/5")
  echo "" >> "$f"
  echo Mean score "$mean" >> "$f"
  echo "" >> "$f"
done  