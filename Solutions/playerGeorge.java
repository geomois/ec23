package Solutions;
import org.vu.contest.ContestEvaluation;
import org.vu.contest.ContestSubmission;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer;

public class playerGeorge implements ContestSubmission{
	
	Random rand;
	ContestEvaluation evaluation;
    private int evalLimit;
	
	public playerGeorge(){
		rand= new Random();
	}

	@Override
	public void run() {
        int evals = 0;
        while(evals<evalLimit){
            // Select parents
            // Apply crossover / mutation operators
            double child[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
            // Check fitness of unknown fuction
            Double fitness = (double) evaluation.evaluate(child);
            evals++;
            // Select survivors
        }
	}

	@Override
	public void setEvaluation(ContestEvaluation eval) {
		
		evaluation = eval;
		Properties funcProps= eval.getProperties();

		evalLimit=Integer.parseInt(funcProps.getProperty("Evaluations"));
		boolean isMultimodal = Boolean.parseBoolean(funcProps.getProperty("Multimodal"));
        boolean isRegular= Boolean.parseBoolean(funcProps.getProperty("Regular"));
        boolean isSeparable = Boolean.parseBoolean(funcProps.getProperty("Separable"));
        
        if(isMultimodal){
        }else{
        }
	}

	@Override
	public void setSeed(long seed) {
		rand.setSeed(seed);
	}
}
