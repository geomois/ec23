import numpy as np
import pandas as pd

df = pd.read_csv('scraped_data.csv', parse_dates=[3,4], dayfirst=True)
df.run_time = pd.to_datetime(df.run_time)
ns10min=10*60*1000000000   # 10 minutes in nanoseconds
df.run_time = pd.DatetimeIndex(((df.run_time.astype(np.int64) // ns10min + 1 )\
                                * ns10min))
df.drop('scrape_time', axis=1, inplace=True)
df.sort_values(['player', 'run_time'], inplace=True)
#%%
def only_show_personal_best(col):
    out = []
    for i in range(len(col)):
        out.append(np.max(col[:(i+1)]))
    return out

# We will only show entries from the date where at least cutoff % people have entries
cutoff = 80

dates = pd.date_range(start=min(df.run_time), end=max(df.run_time), freq='10min')

results = {}
best = {}
for f in df.function.unique():
    results[f] = pd.DataFrame(index=dates)
    for p in df.player.unique():
        d = df[np.bitwise_and(df.player==p, df.function==f)][['run_time', 'score']]
        results[f][p] = np.nan
        v = results[f].merge(d, left_index=True, right_on='run_time', how='outer')['score']        
        results[f][p] = v.values
    results[f].fillna(method='ffill', inplace=True)
    
    #d[int(np.percentile(d.isnull().sum(), cutoff)):].plot(title='function ' + str(f))
    # Just going to ignore the first 20000 rows as not much data there...   
    best[f] = results[f].copy()    
    for i in range(len(results[f])):
        best[f].iloc[i, :] = results[f].iloc[:(i+1),:].max(axis=0)
#%%
for f in df.function.unique():
    best[f][2000:].plot(title='function ' + str(f), ylim=[-1, 11], linewidth=2)