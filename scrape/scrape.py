def scrape_to_df(html):
    soup = BeautifulSoup(html, 'lxml')
    table = soup.find("table")
    
    headings = [th.get_text() for th in table.find_all('tr')]
    headings = [[i for i in j.split('\n') if len(i) > 0] for j in headings]
    
    def fix_it(h):
        now = datetime.datetime.now()
        hh = h[1]
        pos = hh.find('(')
        return [h[0], hh[0:pos], hh[(pos+1):-1], now]
        
    headings = [fix_it(h) for h in headings if 'function' not in h[0]]
    
    df = pd.DataFrame(headings,
                      columns=['player', 'score', 'run_time', 'scrape_time'])
    df['score'] = df['score'].astype(np.float)
    
    def determine_function_number(df):
        s = df.score.values
        f = [1]
        curr = 1
        for i in range(1, s.shape[0]):
            if s[i] > s[i-1]:
                curr += 1
            f.append(curr)
        return f
                
    df['function'] = determine_function_number(df)
    df = df[['player', 'function', 'score', 'run_time', 'scrape_time']]
    df_new = pd.read_csv('scraped_data.csv')
    old_len = len(df_new)
    df_new = pd.concat((df, df_new))
    df_new = df_new.drop_duplicates(subset=['player', 'function', 'run_time'])
    return df_new, len(df_new) > old_len

def main(args):
    site = 'http://mac360.few.vu.nl:8080/EC_BB_ASSIGNMENT/rankings.html'
    seconds_wait_time_between_scrapes = 60
    while True:
        sleepcounter = 10
        try:
            html = urlopen(site).read()
            df_new, change_detected = scrape_to_df(html)
            if change_detected:
                print 'Change detected. Writing to csv then waiting', seconds_wait_time_between_scrapes , 'seconds for next scrape...'
                df_new.to_csv('scraped_data.csv', index=False)
            else:
                print 'No change detected. Waiting', seconds_wait_time_between_scrapes, 'seconds for next scrape...'
            time.sleep(seconds_wait_time_between_scrapes)
        except IOError:
            print 'internet connection failed! waiting', sleepcounter, 'seconds...'
            time.sleep(sleepcounter)
            sleepcounter += 1
    return 0

if __name__ == '__main__':
    from urllib import urlopen
    import sys
    import time
    import numpy as np
    import smtplib
    import os
    from bs4 import BeautifulSoup
    import datetime
    import pandas as pd
    sys.exit(main(sys.argv))