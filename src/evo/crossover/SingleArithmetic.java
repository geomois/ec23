package evo.crossover;

import evo.individual.Individual;

import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;


/**
 * Created by liam on 22-9-16.
 */
public class SingleArithmetic extends Crossover {

	private double alpha;
    public SingleArithmetic(Random rnd, int offspringNumber,double alpha){
        super(rnd,offspringNumber);
        this.alpha = alpha;
    }
   

    public ArrayList<Individual> crossover(ArrayList<Individual> parents, int evals) {

        int genomeLength = parents.get(0).getGenome().length;
        ArrayList<Individual> offspring = new ArrayList<Individual>(offspringNumber);
        int j = 0;
        while(j < offspringNumber) {
        	for(int p = 0; p < parents.size(); p += 2){ //every pair of parents in the list
        		double[] newIndividual1 = parents.get(p).getGenome().clone();
 	            double[] newIndividual2 = parents.get(p+1).getGenome().clone();
 	            
 	            int k = rnd.nextInt(genomeLength);
 	           
 	            newIndividual1[k] = alpha*parents.get(p+1).getGenome()[k] + (1-alpha)*parents.get(p).getGenome()[k];
 	            newIndividual2[k] = alpha*parents.get(p).getGenome()[k] + (1-alpha)*parents.get(p+1).getGenome()[k];
	           
	           
	            Individual ind = new Individual(parents.get(0).getDims(), parents.get(0).getUB(), parents.get(0).getLB());
	            ind.setGenome(newIndividual1);
	            offspring.add(ind);
	            ind = new Individual(parents.get(0).getDims(), parents.get(0).getUB(), parents.get(0).getLB());
	            ind.setGenome(newIndividual2);
	            offspring.add(ind);
	            
	            j+=2;
        	}
        }

        return offspring;
    }

}
