package evo.crossover;

import evo.individual.Individual;

import java.util.ArrayList;

import java.util.Random;


/**
 * Created by liam on 22-9-16.
 */
public class Discrete extends Crossover {

    public Discrete(Random rnd, int offspringNumber){
        super(rnd,offspringNumber);
    }

    public ArrayList<Individual> crossover(ArrayList<Individual> parents, int evals) {

        int genomeLength = parents.get(0).getGenome().length;
        ArrayList<Individual> offspring = new ArrayList<Individual>(offspringNumber);
        int j = 0;
        while(j < offspringNumber) {
        	for(int p = 0; p < parents.size(); p += 2){
	            double[] newIndividual = new double[genomeLength];
	            for (int i = 0; i < genomeLength; i++) {
	                if (rnd.nextDouble() <= 0.5)
	                    newIndividual[i] = parents.get(p).getGenome()[i];
	                else
	                    newIndividual[i] = parents.get(p+1).getGenome()[i];
	            }
	
	            Individual ind = new Individual(parents.get(0).getDims(), parents.get(0).getUB(), parents.get(0).getLB());
	            ind.setGenome(newIndividual);
	            offspring.add(ind);
	            
	            j++;
        	}
        }

        return offspring;
    }

}
