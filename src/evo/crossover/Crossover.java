package evo.crossover;

import evo.individual.Individual;

import java.util.ArrayList;
import java.util.Random;

public abstract class Crossover {
	protected Random rnd;
	protected int    offspringNumber;

	public Crossover(Random rnd, int offspringNumber){
		this.rnd = rnd;
		this.offspringNumber = offspringNumber;
	}
	public abstract ArrayList<Individual> crossover(ArrayList<Individual> parents, int evals);

}
