package evo.crossover;


import evo.EigenvalueDecomposition;
import evo.individual.Individual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import static evo.LinAlgStuff.*;

public class CMAES extends Crossover {

//    private MultivariateNormalDistribution mnd;

    // Crossover class will store the 'shared genome'
    private double[][] mean;
    private double[][] meanOld;
    private double sigma;
    private int dims;
    private int mu;
    private int lambda;
    private double[][] weights;
    private double[][] X;
    private double[][] pc;
    private double[][] ps;
    private double[][] D;
    private double[][] B;
    private double[][] C;
    private double[][] invsqrtC;
    private double chiN = 0.0;

    private double mueff;
    private double cc;
    private double cs;
    private double c1;
    private double cmu;
    private double ds;
    private double hs;


    public CMAES(Random rnd, int dims, double sigmaInit,
                          int offspringNumber, int lambda, int mu, ArrayList<Individual> pop) {
    	
        super(rnd, offspringNumber);

      
        this.dims   = dims;
        this.mu     = mu;
        this.lambda = lambda;
        
        // Initialise means for CMA-ES
        double[] meansInit = new double[dims];
        for (int i = 0; i < dims; i++) {
            meansInit[i] = rnd.nextDouble() - 0.5; //write about
        }
        
        sigma = sigmaInit;
        mean  = vecToMatrix(meansInit);


        // Create weights
        double[] v = new double[mu];
        double sum = 0.0;
        
        for (int i = 0; i < mu; i++) {
            v[i] = Math.log(lambda*0.5+0.5) - Math.log(i + 1.0); 
            sum += v[i];
        }
        
        weights = vecToMatrix(v);
        weights = mMult(this.weights, 1.0 / sum);
    
        mueff = 1.0 / mSumSq(this.weights);
        cc    = (4 + mueff / dims) / (dims + 4 + 2 * mueff / dims);
        cs    = (mueff + 2) / (dims + mueff + 5);
        c1    = 2 / (Math.pow((dims + 1.3), 2) + mueff);
        cmu   = Math.min(1 - this.c1, 2 * (mueff - 2 + 1 / mueff) / ((Math.pow(dims + 2, 2) + mueff)));
        ds    = 1 + 2 * Math.max(0.0, Math.sqrt((mueff - 1.0) / (dims + 1)) - 1) + this.cs;

        ps    = vecToMatrix(getZeroVector(dims));
        pc    = vecToMatrix(getZeroVector(dims));
        B     = getIdentityMatrix(dims);
        D     = vecToMatrix(getOnesVector(dims));

        double[][] dDiag = vecToDiagonalMatrix(mElwiseMult(this.D, this.D));
        
        C        = mMult(mMult(this.B, dDiag), mTranspose(this.B));
        dDiag    = vecToDiagonalMatrix(inverseOfEachElement(this.D));
        invsqrtC = mMult(mMult(this.B, dDiag), mTranspose(this.B));
        chiN     = Math.pow(dims, 0.5) * (1.0 - 1.0 / (4 * dims) + 1.0 / (21.0 * Math.pow(dims, 2)));

    }
   

    public void updateMean(ArrayList<Individual> pop) {
        // Take a weighted mean of the mu best guys
        meanOld = mean;

        // Transposing so we can set the columns
        X = new double[mu][dims];
        for (int i = 0; i < mu; i++) {
            X[i] = pop.get(i).getPhenome();
        }
        X = mTranspose(X);
        mean = mMult(X, weights);
    }

    public void updatePsPc(int evals) {
        double[][] rhs    = mMult(mMult(invsqrtC, mElwiseSubtract(mean, meanOld)), Math.sqrt(cs * (2.0 - cs) * mueff) / sigma);
        double[][] tempPs = mElwiseAdd(mMult(ps, 1.0 - cs), rhs);
        
        boolean skip = false;
        for(int i = 0; i < tempPs.length; i++){ //ps computations sometimes grows to much
        	if(Double.isInfinite(tempPs[i][0]) || Double.isNaN(tempPs[i][0])){ 
        		skip = true;
        		break;
        	}
        }
        if(!skip) ps = tempPs;
        hs = vecNorm(ps) / Math.sqrt(1 - Math.pow(1 - cs, 2 * evals / lambda)) / chiN < 1.4 + 2.0 / (dims + 1) ? 1.0 : 0.0;
        pc = mElwiseAdd(mMult(pc, 1.0 - cc), mMult(mElwiseSubtract(mean, meanOld), hs * Math.sqrt(cc * (2 - cc) * mueff) / sigma));
    }

    private void updateC() {
    	
        double[][] XOld  = vecRepeat(meanOld, mu);
        double[][] artmp = mMult(mElwiseSubtract(X, XOld), 1.0 / sigma);
        double[][] wDiag = vecToDiagonalMatrix(weights);
        double[][] t1    = mMult(C, 1 - c1 - cmu);
        double[][] t20   = mMult(C, (1 - hs) * cc * (2 - cc));
        double[][] t21   = mMult(pc, mTranspose(pc));
        double[][] t2    = mMult(mElwiseAdd(t21, t20), c1);
        double[][] t3    = mMult(mMult(mMult(artmp, wDiag), mTranspose(artmp)), cmu);
        
        C = mElwiseAdd(t1, mElwiseAdd(t2, t3));
        
    }

    private void updateSigma() {
        sigma = sigma * Math.exp((cs / ds) * (vecNorm(ps) / chiN - 1));
    }

    public ArrayList<Individual> crossover(ArrayList<Individual> pop, int evals) {

        Collections.sort(pop);
        updateMean(pop);
        updatePsPc(evals);
        updateC();
        updateSigma();
        doDecomp();

        // Create phenomes by sampling from the updated distribution
        
        for (int i = 0; i < pop.size(); i++) { 	
	    	double[][] rStandNorm  = getStdNormVec(dims, rnd);
	    	double[][] DrStandNorm = mElwiseMult(D, rStandNorm);
	    	double[][] newPhenome  = mElwiseAdd(mMult(mMult(B, DrStandNorm), sigma), mean);
	        pop.get(i).setGenome(matrixToVector(newPhenome));
        }

        return pop;
    }

    private void doDecomp() {
    	
		C = mElwiseAdd(upperTriangular(C,0),mTranspose(upperTriangular(C,1)));
		
		EigenvalueDecomposition evd = new EigenvalueDecomposition(C);
		
		double[][] tempB = evd.getV();
		double[][] vd    = mSqrt(evd.getD());
		double[][] tempD = D;
		
		boolean skip = false;
		for(int i = 0; i < D.length; i++){
			double val = vd[i][i];
			if(Double.isNaN(val)){ //problems with negative eigenvalues, it happens rarely
				skip = true;
			    break;
			}
			tempD[i][0] = vd[i][i];
		}
        if(!skip){
        	D = tempD;
        	B = tempB;
        }
        double[][] dDiag = vecToDiagonalMatrix(inverseOfEachElement(D));
        
	    invsqrtC = mMult(mMult(this.B, dDiag), mTranspose(this.B));

    }

	

}