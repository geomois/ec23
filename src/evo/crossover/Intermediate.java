package evo.crossover;

import evo.individual.Individual;

import java.util.ArrayList;

import java.util.Random;


/**
 * Created by liam on 22-9-16.
 */
public class Intermediate extends Crossover {

    public Intermediate(Random rnd, int offspringNumber){
        super(rnd,offspringNumber);
    }
   

    public ArrayList<Individual> crossover(ArrayList<Individual> parents, int evals) {

        int genomeLength = parents.get(0).getGenome().length;
        ArrayList<Individual> offspring = new ArrayList<Individual>(offspringNumber);
        int j = 0;
        while(j < offspringNumber) {
        	for(int p = 0; p < parents.size(); p += 2){ //every pair of parents in the list
	            double[] newIndividual1 = new double[genomeLength];
	            double[] newIndividual2 = new double[genomeLength];
	        	double a = rnd.nextDouble();
	            for (int i = 0; i < genomeLength; i++) {
	                newIndividual1[i] = a*parents.get(p).getGenome()[i] + (1-a)*parents.get(p+1).getGenome()[i];
	                newIndividual2[i] = a*parents.get(p+1).getGenome()[i] + (1-a)*parents.get(p).getGenome()[i];
	            }
	
	            Individual ind = new Individual(parents.get(0).getDims(), parents.get(0).getUB(), parents.get(0).getLB());
	            ind.setGenome(newIndividual1);
	            offspring.add(ind);
	            ind = new Individual(parents.get(0).getDims(), parents.get(0).getUB(), parents.get(0).getLB());
	            ind.setGenome(newIndividual2);
	            offspring.add(ind);
	            
	            j+=2;
        	}
        }

        return offspring;
    }

}
