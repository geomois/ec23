package evo.selection;

import evo.individual.Individual;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class DoNothingSelection extends Selection {

    public DoNothingSelection(){}

    public  ArrayList<Individual> select(ArrayList<Individual> population, int offspringNumber){

        return population;

    }

}
