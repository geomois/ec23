package evo.selection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import evo.individual.Individual;

public class RouletteWheel extends Selection {

	public static final int CUMULATIVE = 0, STOCHASTIC = 1;
	private Random rnd;
	private int method;
	
	
	public RouletteWheel(Random rnd, int method){
		this.rnd    = rnd;
		this.method = method;
	}
	
	public ArrayList<Individual> cumulative(ArrayList<Individual> population,int offspringNumber){
		
		double[] cumulativeFitness = new double[population.size()];
		double   min               = 0.0;
		
		//we need the minimum fitness to deal with negative values;
		for(int i = 0; i < population.size(); i++)
			if (population.get(i).getFitness() < min) min = population.get(i).getFitness();
		
		min                  = Math.abs(min);
		cumulativeFitness[0] = population.get(0).getFitness()+min;
		
		for(int i = 1; i < population.size(); i++){
			cumulativeFitness[i] = cumulativeFitness[i-1] + population.get(i).getFitness()+min;

		}
		
		ArrayList<Individual> offspring = new ArrayList<Individual>(offspringNumber);
        for (int i = 0; i < offspringNumber; i++) {
        	
        	double randomFitness = rnd.nextDouble()*cumulativeFitness[cumulativeFitness.length-1];
        	int index            = Arrays.binarySearch(cumulativeFitness, randomFitness);
        	
        	if(index < 0)  index = Math.abs(index + 1);
        	
            offspring.add(population.get(index));
            
        }

        return offspring;
	}
	
	public ArrayList<Individual> stochastic(ArrayList<Individual> population,int offspringNumber) {

        double min        = 0.0;
        double maxFitness = 0.0;
		
		//we need the minimum fitness to deal with negative values;
		for(int i = 0; i < population.size(); i++)
			if (population.get(i).getFitness() < min) min = population.get(i).getFitness();
			else if (population.get(i).getFitness() > maxFitness) maxFitness = population.get(i).getFitness();
		
		min        = Math.abs(min)+1;
		maxFitness = maxFitness+min;

		ArrayList<Individual> offspring = new ArrayList<Individual>(offspringNumber);
		
        for (int i = 0; i < offspringNumber; i++) {
        	boolean rejected  = true;
        	int     index     = 0; 
        	while(rejected){
        		index = rnd.nextInt(population.size());
        		if(rnd.nextDouble() < (population.get(index).getFitness()+min)/maxFitness) rejected = false;
        	}
        	offspring.add(population.get(index));
        }

        return offspring;
		
		
	}

	@Override
	public ArrayList<Individual> select(ArrayList<Individual> population, int offspringNumber) {
		
		switch(method){
			case CUMULATIVE: return cumulative(population,offspringNumber);
			case STOCHASTIC: return stochastic(population,offspringNumber);
		}
		return cumulative(population,offspringNumber);
	}

}