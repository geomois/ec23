package evo.selection;

import evo.individual.Individual;

import java.util.ArrayList;

public abstract class Selection {

	public abstract ArrayList<Individual> select(ArrayList<Individual> population, int nOffspring);

}
