package evo.selection;

import evo.individual.Individual;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by liam on 22-9-16.
 */
public class Elitism extends Selection {

    public Elitism(){}

    public  ArrayList<Individual> select(ArrayList<Individual> population, int offspringNumber){

        ArrayList<Individual> offspring = new ArrayList<Individual>(offspringNumber);
        Collections.sort(population);

        for (int i = 0; i < offspringNumber; i++) {
            offspring.add(population.get(i));
        }

        return offspring;

    }

}
