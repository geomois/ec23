package evo.mutation;

import evo.individual.Individual;


import java.util.Random;

public class DoNothingMutation extends Mutation{


    public DoNothingMutation(Random rnd){
        super(rnd);

    }


    public Individual mutate(Individual individual){

        return individual;

    }
}
