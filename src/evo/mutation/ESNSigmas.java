package evo.mutation;

import evo.individual.Individual;

import java.util.Random;

/**
 * Created by liam on 22-9-16.
 */
public class ESNSigmas extends Mutation{

    private double mutationRate;
    private double eta;
    private double tau1;
    private double tau2;
    private int    dims;

    public ESNSigmas(Random rnd, double eta, double mutationRate, int dims){
        super(rnd);
        this.eta          = eta;
        this.mutationRate = mutationRate;
        this.dims         = dims;
        tau2 = 1.0 / Math.sqrt(2.0 * dims);
        tau1 = 1.0 / Math.sqrt(2.0 * Math.sqrt(dims));

    }


    public Individual mutate(Individual individual){

        double[] newIndividual = individual.getGenome();
        double UB 			   = individual.getUB();
        double LB              = individual.getLB();
        double globGauss       = rnd.nextGaussian();
       
        
        for (int i = 0; i < dims; i++) {
        	if(rnd.nextDouble() < mutationRate){
	        	double itGauss = rnd.nextGaussian();
	        	newIndividual[dims+i] *= Math.exp(globGauss * tau2 + itGauss * tau1);
	            if (newIndividual[dims+i] < eta) {
	            	newIndividual[dims+i] = eta;
	            }
	            newIndividual[i] += newIndividual[dims+i] * itGauss;
	            if(newIndividual[i] > UB) newIndividual[i] = UB;
                else if(newIndividual[i] < LB) newIndividual[i] = LB;
        	}
        }

        Individual ind = new Individual(individual.getDims(), individual.getUB(), individual.getLB());
        ind.setGenome(newIndividual);
        return ind;

    }

}
