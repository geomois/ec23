package evo.mutation;

import evo.individual.Individual;

import java.util.Random;

/**
 * Created by liam on 22-9-16.
 */
public class ESSingleSigma extends Mutation{

    private double mutationRate;
    private double eta;
    private double tau;
    private int    dims;

    public ESSingleSigma(Random rnd, double eta, double mutationRate, int dims){
        super(rnd);
        this.eta          = eta;
        this.mutationRate = mutationRate;
        this.tau          = 1.0/Math.sqrt((double)dims);
        this.dims         = dims;

    }


    public Individual mutate(Individual individual){

        double[] newIndividual = individual.getGenome();
        
        newIndividual[dims] *= Math.exp(tau*rnd.nextGaussian());
        
        if (newIndividual[dims] < eta) newIndividual[dims] = eta;
        
        for (int i = 0; i < dims; i++){
            if(rnd.nextDouble() < mutationRate){
                newIndividual[i] +=  rnd.nextGaussian()*newIndividual[dims];
               
            }

        }

        Individual ind = new Individual(individual.getDims(), individual.getUB(), individual.getLB());
        ind.setGenome(newIndividual);
        return ind;

    }

}
