package evo.mutation;

import evo.individual.Individual;

import java.util.Random;

/**
 * Created by liam on 22-9-16.
 */
public class DeltaNonUniformMutation extends Mutation{

    private double mutationRate;
    private int    T;
    private int    stepSize;
    private int    t = 1;
    private int    counter = 0;
    private double b;

    public DeltaNonUniformMutation(Random rnd, int eval_limit, int stepSize, double mutationRate, double b){
        super(rnd);
        this.mutationRate = mutationRate;
        this.T            = eval_limit/stepSize;
        this.stepSize     = stepSize;
        this.b            = b;
    }


    public Individual mutate(Individual individual){
    	
    	if(counter == stepSize){
    		counter = 0;
    		t++;
    	}else{
    		counter++;
    	}
    	double[] newIndividual = individual.getGenome();
		double UB = individual.getUB();
		double LB = individual.getLB();
		for (int i = 0; i < individual.getDims(); i++){
			if(rnd.nextDouble() < mutationRate){
				if(rnd.nextBoolean()){
					newIndividual[i] += delta(t, UB - newIndividual[i]);
				}else{
					newIndividual[i] -= delta(t, newIndividual[i] - LB);
				}
				if(newIndividual[i] > UB) newIndividual[i] = UB;
				else if(newIndividual[i] < LB) newIndividual[i] = LB;

			}

		}
		
		Individual ind = new Individual(individual.getDims(), individual.getUB(), individual.getLB());
        ind.setGenome(newIndividual);
        return ind;

    }


    private double delta(int t, double y) {
		return (y*(1.0-Math.pow(rnd.nextDouble(),Math.pow((1.0 -t/(double) T), b))));
	}

}
