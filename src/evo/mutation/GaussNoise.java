package evo.mutation;

import evo.individual.Individual;

import java.util.Random;

/**
 * Created by liam on 22-9-16.
 */
public class GaussNoise extends Mutation{

    private double std;
    private double mutationRate;

    public GaussNoise(Random rnd, double std, double mutationRate){
        super(rnd);
        this.std = std;
        this.mutationRate = mutationRate;
    }


    public Individual mutate(Individual individual){

        double[] newIndividual = individual.getGenome();
        double UB = individual.getUB();
        double LB = individual.getLB();
        for (int i = 0; i < individual.getDims(); i++){
            if(rnd.nextDouble() < mutationRate){
                newIndividual[i] +=  rnd.nextGaussian()*std;
                if(newIndividual[i] > UB) newIndividual[i] = UB;
                else if(newIndividual[i] < LB) newIndividual[i] = LB;
            }

        }

        Individual ind = new Individual(individual.getDims(), individual.getUB(), individual.getLB());
        ind.setGenome(newIndividual);
        return ind;

    }

}
