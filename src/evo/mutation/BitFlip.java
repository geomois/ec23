package evo.mutation;

import java.math.BigInteger;
import java.util.Random;

import evo.individual.Individual;

public class BitFlip extends Mutation {

	public BitFlip(Random rnd) {
		super(rnd);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Individual mutate(Individual individual) {
		
	double[] newIndividual = individual.getGenome();
	double UB = individual.getUB();
	double LB = individual.getLB();	
	for (int i = 0; i < newIndividual.length; i++){
		if(rnd.nextDouble() < 0.1){
			String s = Long.toBinaryString(Double.doubleToRawLongBits(newIndividual[i]));
			int index = rnd.nextInt(s.length()); //just random bit flip (1 bit only)
			s = s.substring(0, index) + flip(s.substring(index, index+1)) + s.substring(index+1, s.length());
			newIndividual[i] = Double.longBitsToDouble(new BigInteger(s, 2).longValue());
			if(newIndividual[i] > UB) newIndividual[i] = UB;
			else if(newIndividual[i] < LB) newIndividual[i] = LB;
		}
	}
	
	Individual ind = new Individual(individual.getDims(), individual.getUB(), individual.getLB());
    ind.setGenome(newIndividual);
    return ind;
		
	}
	private String flip(String s){
		return s.equals("0")? "1":"0";
	}
	

}
