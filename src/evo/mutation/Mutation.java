package evo.mutation;

import evo.individual.Individual;

import java.util.Random;

public abstract class Mutation {

	protected Random rnd;

	public Mutation(Random rnd){
		this.rnd = rnd;
	}

	public abstract Individual mutate(Individual individual);

	
}
