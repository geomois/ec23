package evo;


import evo.crossover.Crossover;
import evo.individual.Individual;
import evo.mutation.Mutation;
import evo.selection.Selection;
import org.vu.contest.ContestEvaluation;

import java.util.ArrayList;

public class GeneticAlgorithm {

	  private ArrayList<Individual> population; 
	  private Mutation              mutation;
	  private Crossover             combination;
	  private Selection             parentSelection;
	  private Selection             survivorSelection;
	  private ContestEvaluation     evaluation;
	  private int                   eval_limit;
	  private int                   evals = 0;
	  private int                   populationSize;
	  private int                   parentNumber;
	  public double                 bestFitness = -100.0;
	  private int                   restartLimit;
	  private int                   t = 0;
	  
	  /*We will initialize all the operator including the population in player23, we can specify there
	   *what implementations (individual, Mutation, etc.) we want to use*/
	  public GeneticAlgorithm(ArrayList<Individual> population, Mutation mutation, Crossover combination,
			                  Selection parentSelection, Selection survivorSelection, ContestEvaluation evaluation, 
			                  int eval_limit, int populationSize, int parentNumber, int restartLimit){
		  
			this.population        = population;
			this.mutation          = mutation;
			this.combination       = combination;
			this.parentSelection   = parentSelection;
			this.survivorSelection = survivorSelection;
			this.evaluation        = evaluation;
			this.eval_limit        = eval_limit;
			this.populationSize    = populationSize;
			this.parentNumber      = parentNumber;
			this.restartLimit      = restartLimit;
			
	  }
	  
	  public ArrayList<Individual> parentSelection(){
		  return parentSelection.select(population, parentNumber);
	  }
	  
	  public ArrayList<Individual> crossover(ArrayList<Individual> parents){
		  return combination.crossover(parents,evals);
	  }
	  
	  public Individual mutate(Individual individual){
		  return mutation.mutate(individual);
	  }
	  
	  public ArrayList<Individual> survivorSelection(ArrayList<Individual> offspring){
		  return survivorSelection.select(offspring,populationSize);
	  }
	
	  public void setEvals(int evals){
		  this.evals = evals;
	  }
	  public int getEvals(){
		  return evals;
	  }
	  
	  public void evaluate(ArrayList<Individual> population){
		  
		  for (int i = 0; i < population.size(); i++){ //evaluate fitness
			  if (evals == eval_limit) break;

			  Individual ind = population.get(i);
			  double fitness = (Double) evaluation.evaluate(ind.getPhenome());
			  ind.setFitness(fitness);
	          evals++;
	          if(fitness > bestFitness) bestFitness = fitness;
	          
  		  }
	  }
      
	  public double run(){
		  double currentFitness = -100.0;
		  evaluate(population);
		  while(evals < eval_limit){
			  ArrayList<Individual> parents   = parentSelection();
			  ArrayList<Individual> offspring = crossover(parents);

			  ArrayList<Individual> mutated_offspring = new ArrayList<Individual>(offspring.size());
			  for(int i = 0; i < offspring.size(); i++){
				  mutated_offspring.add(mutate(offspring.get(i)));
			  }
			  
			  evaluate(mutated_offspring);
			  population = survivorSelection(mutated_offspring);
			  
			  if (restartLimit > 0) {
				  if (t % restartLimit == 0 && bestFitness <= currentFitness){
					  return bestFitness;
				  }else{
					  currentFitness = bestFitness;
				  }
			  }
			  t++;
			 
			  
		  }
		  return bestFitness;
		  
	  }

	
}
