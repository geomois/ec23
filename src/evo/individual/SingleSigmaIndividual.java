package evo.individual;

import java.util.Random;

public class SingleSigmaIndividual extends Individual{

	public SingleSigmaIndividual(Random rnd, int dims, double UB, double LB) {

		super(dims,UB,LB);
		genome  = new double[dims+1];
		
    	for(int i = 0; i < dims; i++){
    		genome[i] = rnd.nextDouble()*(UB-LB) + LB;
    	}
    	genome[dims] = rnd.nextDouble() + 0.5; //init standard deviation
		
	}

}
