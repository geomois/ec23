package evo.individual;

import java.util.Random;

public class Individual implements Comparable<Individual>{

	protected double   fitness  = 0.0;
	protected double[] genome;
	protected double[] phenome;
	protected int      dims;
	protected double   UB;
	protected double   LB;

    public Individual(int dims, double UB, double LB){
        this.dims = dims;
        this.UB   = UB;
        this.LB   = LB;
    }
	
	/*Implementations of this class will have to override the constructor if the genome is longer than dims or
	 *if it follows a different initialization than the default class*/
	public Individual(Random rnd, int dims, double UB, double LB){
		
		this.dims = dims;
		this.UB   = UB;
		this.LB   = LB;
		
		genome  = new double[dims];
		
    	for(int i = 0; i < dims; i++){
    		genome[i] = rnd.nextDouble()*(UB-LB) + LB;
    	}
	}
	
	/*We have to override this method for implementations of Individual with genome different than phenome 
	 * unless the genome is an double array with the phenome in the first dims positions*/
	public double[] getPhenome() {
		phenome = new double[dims];
		for(int i = 0; i < dims; i++){
			phenome[i] = genome[i];
		}
		return phenome; 
	}

	public void setPhenome(double[] phenome) {this.phenome = phenome;}

	public void setGenome(double[] genome){
		this.genome = genome;
	}
	
	public double[] getGenome(){
		return genome;
	}

	public double getFitness() {
		return fitness;
	}

	public void setFitness(double fitness) {
		if(Double.isNaN(fitness)) fitness = -9999999.0;
		this.fitness = fitness;
	}

	public int compareTo(Individual indv) {
		return new Double(indv.getFitness()).compareTo(fitness);
	}

	public double getUB(){
		return UB;
	}

	public double getLB(){
		return LB;
	}

	public int getDims(){
		return dims;
	}

}
