package evo.individual;

import java.util.Random;

public class NSigmaIndividual extends Individual{

	public NSigmaIndividual(Random rnd, int dims, double UB, double LB) {

		super(dims,UB,LB);
		genome  = new double[dims*2];
		
    	for(int i = 0; i < dims; i++){
    		genome[i] = rnd.nextDouble()*(UB-LB) + LB;
    		genome[dims+i] = rnd.nextDouble() + 0.5; //init standard deviation
    	}
    	
		
	}

}
