package evo;

import java.util.Random;

/**
 * Created by liam on 2-10-16.
 */

public class LinAlgStuff {


    public static double vecNorm(double[] v) {
        return Math.sqrt(mSumSq(v));
    }

    public static double vecNorm(double[][] v) {
        return Math.sqrt(mSumSq(v));
    }

    public static double[][] vecToMatrix(double[] m) {
        double[][] temp = new double[m.length][1];
        for (int i = 0; i < m.length; i++) {
            temp[i][0] = m[i];
        }
        return temp;
    }

    public static double mSumSq(double[] m) {
        return mSumSq(vecToMatrix(m));
    }

    public static double mSumSq(double[][] m) {
        double sum = 0.0;
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                sum += m[i][j] * m[i][j];
            }
        }
        return sum;
    }

    public static double[][] mTranspose(double[][] m) {
        double[][] temp = new double[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }


    public static double[][] inverseOfEachElement(double[][] A) {
        int aRows = A.length;
        int aColumns = A[0].length;

        double[][] C = new double[aRows][aColumns];

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < aColumns; j++) { // aColumn
            	double val = A[i][j];
                if(val != 0) C[i][j] = 1.0 / A[i][j];
            }
        }
        return C;
    }
    public static double[] inverseOfEachElement(double[] v) {

        for (int i = 0; i < v.length; i++) { // aRow
           v[i] = 1.0/v[i];
        }
        return v;
    }
    public static double[] vSqrt(double[] v) {

        for (int i = 0; i < v.length; i++) { // aRow
           v[i] = Math.sqrt(v[i]);
        }
        return v;
    }
    public static double[][] mSqrt(double[][] A) {
    	 int aRows = A.length;
         int aColumns = A[0].length;

         for (int i = 0; i < aRows; i++) { // aRow
             for (int j = 0; j < aColumns; j++) { // aColumn
                 A[i][j] = Math.sqrt(A[i][j]);
             }
         }
         return A;
    }
    

    public static double[][] mMult(double[][] A, double scalar) {
        int aRows = A.length;
        int aColumns = A[0].length;

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < aColumns; j++) { // aColumn
                A[i][j] = A[i][j] * scalar;
            }
        }
        return A;
    }
    public static double[][] mElwiseAdd(double[][] A, double scalar) {
        int aRows = A.length;
        int aColumns = A[0].length;

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < aColumns; j++) { // aColumn
                A[i][j] = A[i][j] + scalar;
            }
        }
        return A;
    }

    public static double[][] mMult(double[][] A, double[][] B) {

    	int m1 = A.length;
        int n1 = A[0].length;
        int m2 = B.length;
        int n2 = B[0].length;
        
        if (n1 != m2) throw new IllegalArgumentException("Illegal matrix dimensions.");
        
        double[][] C = new double[m1][n2];
        double[] Bcolj = new double[n1];
        
        for(int j = 0; j < n2; j++){
        	for(int k = 0; k < n1; k++){
        		Bcolj[k] = B[k][j];
        	}
        	for(int i = 0; i < m1; i++){
        		double[] Arowi = A[i];
        		double sum = 0.0;
        		for(int k = 0; k < n1; k++){
            		sum += Arowi[k]*Bcolj[k];
            	}
        		C[i][j] = sum;
        	}
        }
        return C;
    }

    public static double[][] mElwiseAdd(double[][] A, double[][] B) {

        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        if (aColumns != bColumns || aRows != bRows) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns +
                    " does not equal B size: " + bRows + ", " + bColumns + ".");
        }

        double[][] C = new double[aRows][aColumns];

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < aColumns; j++) { // aColumn
                C[i][j] = A[i][j] + B[i][j];
            }
        }

        return C;
    }

    public static void printMat(double[][] M) {
        System.out.println("Printing matrix. Shape = " + M.length + "x" + M[0].length);
        for (int i = 0; i < M.length; i++) {
            String line = "";
            for (int j = 0; j < M[0].length; j++) {
                String numstr = Double.toString(M[i][j]);
                line = line + numstr.substring(0, Math.min(numstr.length(), 6)) + " ";
            }
            System.out.println(line);
        }
    }

    public static double[][] vecRepeat(double[][] A, int reps) {

        int aRows = A.length;
        int aColumns = A[0].length;

        if (aColumns > 1) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns + " is not a vector!");
        }

        double[][] C = new double[aRows][reps];

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < reps; j++) { // aColumn
                C[i][j] = A[i][0];
            }
        }

        return C;
    }
    public static double[][] vecRepeat(double[] A, int reps) {

 
        double[][] C = new double[A.length][A.length];

        for (int i = 0; i < A.length; i++) { // aRow
            for (int j = 0; j < A.length; j++) { // aColumn
                C[i][j] = A[i];
            }
        }

        return C;
    }
    public static double mMult(double[] a, double[] b) {
        if (a.length != b.length) throw new IllegalArgumentException("Illegal vector dimensions.");
        double sum = 0.0;
        for (int i = 0; i < a.length; i++)
            sum += a[i]* b[i];
        return sum;
    }

    public static double[][] mElwiseSubtract(double[][] A, double[][] B) {

        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        if (aColumns != bColumns || aRows != bRows) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns +
                    " does not equal B size: " + bRows + ", " + bColumns + ".");
        }

        double[][] C = new double[aRows][aColumns];

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < aColumns; j++) { // aColumn
                C[i][j] = A[i][j] - B[i][j];
            }
        }

        return C;
    }
    public static double[] mElwiseSubtract(double[] A, double[] B) {

 
        if (A.length != B.length ) {
            throw new IllegalArgumentException("Illegal vector sizes");
        }

        double[] C = new double[A.length];

        for (int i = 0; i < A.length; i++) { // aRow
            for (int j = 0; j < A.length; j++) { // aColumn
                C[i] = A[i] - B[i];
            }
        }

        return C;
    }
    public static double[] mElwiseAdd(double[] A, double[] B) {

    	 
        if (A.length != B.length ) {
            throw new IllegalArgumentException("Illegal vector sizes");
        }

        double[] C = new double[A.length];

        for (int i = 0; i < A.length; i++) { // aRow
            for (int j = 0; j < A.length; j++) { // aColumn
                C[i] = A[i] + B[i];
            }
        }

        return C;
    }

    public static double[][] mElwiseMult(double[][] A, double[][] B) {

        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        if (aColumns != bColumns || aRows != bRows) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns +
                    " does not equal B size: " + bRows + ", " + bColumns + ".");
        }

        double[][] C = new double[aRows][aColumns];

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < aColumns; j++) { // aColumn
                C[i][j] = A[i][j] * B[i][j];
            }
        }

        return C;
    }

    public static double[] matrixToVector(double[][] A) {
        int aRows = A.length;
        int aColumns = A[0].length;

        if (aColumns > 1) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns + " is not a vector!");
        }

        double[] vec = new double[aRows];
        for (int i = 0; i < aRows; i++) {
            vec[i] = A[i][0];
        }
        return vec;
    }

    public static double[][] vecToDiagonalMatrix(double[][] A) {
        int aRows = A.length;
        int aColumns = A[0].length;

        if (aColumns > 1) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns + " is not a vector!");
        }

        double[][] matrix = new double[aRows][aRows];

        for (int i = 0; i < aRows; i++) {
            matrix[i][i] = A[i][0];
        }
        return matrix;
    }
    public static double[][] vecToDiagonalMatrix(double[] v) {
    	  double[][] matrix = new double[v.length][v.length];
          for (int i = 0; i < v.length; i++) {
              matrix[i][i] = v[i];
          }
          return matrix;
	}
    public static double[] mDiagToVec(double[][] A) {
       

        
        double[] vec = getZeroVector(A.length);
        for (int i = 0; i < A.length; i++) {
            vec[i] = A[i][i];
        }
        return vec;
    }

    public static double[][] getIdentityMatrix(int size) {
        double[][] matrix = new double[size][size];
        for (int i = 0; i < size; i++) matrix[i][i] = 1;
        return matrix;
    }


    public static double[] getZeroVector(int d) {
        double[] z = new double[d];
        for (int i = 0; i < d; i++) {
            z[i] = 0.0;
        }
        return z;
    }

    public static double[] getOnesVector(int d) {
        double[] z = new double[d];
        for (int i = 0; i < d; i++) {
            z[i] = 1.0;
        }
        return z;
    }

    public static double[][] getStdNormVec(int dims, Random rnd) {
        double[][] matrix = new double[dims][1];
        for (int i = 0; i < dims; i++) {
            matrix[i][0] = rnd.nextGaussian();
        }
        return matrix;
    }

    public static double[][] upperTriangular(double[][] A, int k) {

        int aRows = A.length;
        int aColumns = A[0].length;

        if (aColumns != aRows) {
            throw new IllegalArgumentException("A size: " + aRows + ", " + aColumns + " is not square!");
        }

        double[][] C = new double[aRows][aColumns];

        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < aColumns; j++) {
                C[i][j] = j > (i + k - 1) ? A[i][j] : 0.0;
            }
        }

        return C;
    }
    public static double[] mMult(double[][] A, double[] b) {
        int m = A.length;
        int n = A[0].length;
        if (b.length != n) throw new IllegalArgumentException("Illegal matrix dimensions.");
        double[] c = new double[m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                c[i] += A[i][j] * b[j];
        return c;
    }
	public static double[] mMult(double[] a, double v) {
		double[] c = new double[a.length];
		for (int i = 0; i < a.length; i++)
			c[i] = a[i]*v;
		return c;
    }
}
