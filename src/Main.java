import java.util.Random;

import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

public class Main {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {

		// Tuning grid
		int[] popSizes = {360,400};
//		int[] popSizes = {10};

		String[] evaluations = {"RosenbrockEvaluation","ZakharovEvaluation","GriewankEvaluation","SphereEvaluation","RastriginEvaluation","AckleyEvaluation","FletcherPowellEvaluation"};
//		String[] evaluations = {"RastriginEvaluation"};

		boolean randSearch = false;
		if (randSearch){
			randomSearch();
			System.exit(0);
		}
		int iterations = 2;

		// For each version of parameters being tuned...
		for (int popSize : popSizes) {
			System.out.println("\nPop size   : " + popSize);
			double finalScore = 0.0;

			// For each function...
			for(int e = 0; e < evaluations.length; e++){

				// For each random seed (iteration)
//				System.out.println("Starting " + evaluations[e] + "... \n");
				double sum = 0.0;
				double score = 0.0;
				for (int i = 0; i < iterations; i++) {
					ContestEvaluation eva = (ContestEvaluation) Class.forName(evaluations[e]).newInstance();
					player23 player = new player23();
					player.setSeed(i);
					player.setEvaluation(eva);
					//popsize, #parents, mutationRate,eta,#offspring,alpha
					player.setParameters(popSize, 2, 1.0 / 10.0, 0.05, 20, 0.5);
					player.run();
					double result = eva.getFinalResult();
//					System.out.println("Iteration " + (i + 1) + ": " + result);
					sum += result;
				}
				score = sum / iterations;
				finalScore += score;
				System.out.println("Function " + evaluations[e] + " average score: " + score);
			}
			System.out.println("Overall average score: " + finalScore/evaluations.length);
		}

	}
	public static void randomSearch(){
		ZakharovEvaluation eva = new ZakharovEvaluation();
		double min = 10000000000000.0;
		for(int p = 0 ; p < 100; p ++){
			int numIt = 100000;
			
			double best = 10000000000000.0;
			for(int i = 0; i < numIt; i++){
				double[] x = new double[10];
				for(int j = 0; j < 10; j++){
					x[j] = Math.random()*(5.0-(-5.0)) + (-5.0);
				}
				double current = eva.function(x)-eva.ftarget_;
				if( current < best) best = current;
			}
			if(best < min) min = best;
			System.out.println(best);
		}
		System.out.println(min);
	}

}
