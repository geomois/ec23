
import evo.GeneticAlgorithm;
import evo.crossover.*;
import evo.individual.*;
import evo.mutation.*;
import evo.selection.DoNothingSelection;
import evo.selection.Elitism;
import evo.selection.RouletteWheel;
import evo.selection.Selection;

import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.util.*;

public class player23 implements ContestSubmission {
    Random rnd_;
    ContestEvaluation evaluation_;
    private int evaluations_limit_;
    private boolean isMultimodal;
    private boolean hasStructure;
    private boolean isSeparable;
    private int     populationSize  = 360;
    private int     parentNumber    = 4;
    private int     dims            = 10;
    private double  mutationRate    = 1.0/dims; 
    private double  eta             = 0.05;
    private int     offspringNumber = 24;
    private double  alpha           = 0.5;


    public player23() {
        rnd_ = new Random();
    }

    public void setSeed(long seed) {
        // Set seed of algorithms random process
        rnd_.setSeed(seed);
    }

    public void setEvaluation(ContestEvaluation evaluation) {
        // Set evaluation problem used in the run
        evaluation_ = evaluation;

        // Get evaluation properties
        Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
        // Property keys depend on specific evaluation
        // E.g. double param = Double.parseDouble(props.getProperty("property_name"));
        isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
        hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
        isSeparable  = Boolean.parseBoolean(props.getProperty("Separable"));


    }

    public void setParameters(int populationSize, int parentNumber, double mutationRate, double eta,
    		                  int offspringNumber, double alpha){
    	this.populationSize  = populationSize;
    	this.parentNumber    = parentNumber;
    	this.mutationRate    = mutationRate;
    	this.eta             = eta;
    	this.offspringNumber = offspringNumber;
    	this.alpha           = alpha;
    }
   
    public void run() {

    	int restartLimit = 50;
    	if(!isMultimodal) restartLimit = 0;
    	
        double sigmaInit = 1.0;
        int lambda       = populationSize;
        int mu           = (int) Math.floor(lambda / 2.0);

        ArrayList<Individual> population = new ArrayList<Individual>(populationSize);

        for (int i = 0; i < populationSize; i++) {
            Individual ind = new Individual(rnd_,dims, 5.0, -5.0);
            population.add(ind);
        }

        Mutation  mutation         = new DoNothingMutation(rnd_);
        Crossover crossover        = new CMAES(rnd_,dims,sigmaInit,offspringNumber,lambda,mu,population);
        Selection parentSelector   = new DoNothingSelection();
        Selection survivorSelector = new DoNothingSelection();

        GeneticAlgorithm ga = new GeneticAlgorithm(population,mutation,crossover,parentSelector,survivorSelector,
                                                   evaluation_,evaluations_limit_,populationSize,parentNumber,
                                                   restartLimit);
        ga.run();
        int currentEvals = ga.getEvals();
        int restarts = 0;
        int cuttOff  = 5;
        while (currentEvals < evaluations_limit_){
        	restarts++;
        	if(restarts<=cuttOff){ //switch algo if it's not working
            	restartLimit = 50;

        		population = new ArrayList<Individual>(populationSize);
                for (int i = 0; i < populationSize; i++) {
					Individual ind = new Individual(rnd_,dims, 5.0, -5.0);
					population.add(ind);
                }
                crossover = new CMAES(rnd_,dims,sigmaInit,offspringNumber,lambda,mu,population);
        	 }else{
             	restartLimit = 70;
        		populationSize = 12;
             	population = new ArrayList<Individual>(populationSize);
     	        for (int i = 0; i < populationSize; i++) {
     	        	SingleSigmaIndividual ind = new SingleSigmaIndividual(rnd_,dims,5.0, -5.0);
     	        	population.add(ind);
     	        }
     	        mutation         = new ESSingleSigma(rnd_,eta,mutationRate,dims);
     	        crossover        = new SingleArithmetic(rnd_,offspringNumber,alpha);
     	        parentSelector   = new RouletteWheel(rnd_, 0);
     	        survivorSelector = new Elitism();
        	 }
        	 ga = new GeneticAlgorithm(population,mutation,crossover,parentSelector,survivorSelector,
	                                   evaluation_,evaluations_limit_,populationSize,parentNumber,restartLimit);
	         ga.setEvals(currentEvals);
             ga.run();
             currentEvals = ga.getEvals();
        }
    }
}
